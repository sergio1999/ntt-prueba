import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContriesService {

  private baseUrl: string;
  constructor(private http: HttpClient) { 
    this.baseUrl = environment.api
  }

  getCountries(){
    const fullUrl = `${this.baseUrl}all`;
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get(fullUrl)
  }

  getInfoCountry(name: string){
    const fullUrl = `${this.baseUrl}name/${name}`;
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get(fullUrl)
  }

}
