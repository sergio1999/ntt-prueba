import { EventEmitter, Inject, Injectable } from '@angular/core';
import { THEMES, ACTIVE_THEME, Theme } from '../tema/simbolo';

@Injectable({
  providedIn: 'root'
})
export class TemaService {
  themeChange = new EventEmitter<Theme>();
  constructor(
    @Inject(THEMES) public themes: Theme[],
    @Inject(ACTIVE_THEME) public theme: string
  ) { }

  getActiveTheme() {
    const theme = this.themes.find(t => t.name === this.theme);
    if (!theme) {
      throw new Error(`Tema no encontrado: '${this.theme}'`);
    }
    return theme;
  }

  setTheme(name: string) {
    this.theme = name;
    this.themeChange.emit(this.getActiveTheme());
  }
}
