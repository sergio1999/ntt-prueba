import { Theme } from './simbolo';

export const darkTheme: Theme = {
    name: 'dark',
    properties: {
        '--background-header': 'hsl(209, 23%, 22%)',
        '--color-header': 'hsl(0, 0%, 100%)',
        '--background-body': 'hsl(207, 26%, 17%)',
        '--background-cards': 'hsl(209, 23%, 22%)',
        '--color-cards': 'hsl(0, 0%, 100%)',
    }
} 
