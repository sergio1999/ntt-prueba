import { Directive, ElementRef, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TemaService } from '../services/tema.service';
import { Theme } from './simbolo';

@Directive({
  selector: '[app-tema]'
})
export class TemaDirective implements OnInit {
  private unsubscribe = new Subject();
  constructor( private _temaService: TemaService, private _elementRef: ElementRef) { }

  ngOnInit() {
    const active = this._temaService.getActiveTheme();
    if (active) {
      this.updateTheme(active);
    }
    this._temaService.themeChange.pipe(takeUntil(this.unsubscribe))
      .subscribe((theme: Theme) => this.updateTheme(theme));
  }

  updateTheme(theme: Theme) {
    for (const key in theme.properties) {
      this._elementRef.nativeElement.style.setProperty(key, theme.properties[key]);
    }
  }
}
