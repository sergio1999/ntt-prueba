import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemaDirective } from './tema.directive';
import { TemaService } from '../services/tema.service';
import { ACTIVE_THEME, ThemeOptions, THEMES } from './simbolo';



@NgModule({
  declarations: [TemaDirective],
  imports: [CommonModule],
  providers: [TemaService],
  exports: [TemaDirective]
})
export class TemaModule {
  static forRoot(options: ThemeOptions): ModuleWithProviders<any> {
    return {
      ngModule: TemaModule,
      providers: [
        {
          provide: THEMES,
          useValue: options.themes
        },
        {
          provide: ACTIVE_THEME,
          useValue: options.active
        }
      ]
    };
  }
}
