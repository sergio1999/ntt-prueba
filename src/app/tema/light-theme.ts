import { Theme } from './simbolo';

export const lightTheme: Theme = {
    name: 'light',
    properties: {
        '--background-header': 'hsl(0, 0%, 100%)',
        '--color-header': 'hsl(209, 23%, 22%)',
        '--background-body': 'hsl(0, 0%, 98%)',
        '--background-cards': 'hsl(0, 0%, 100%',
        '--color-cards': 'hsl(209, 23%, 22%)',
    }
};
