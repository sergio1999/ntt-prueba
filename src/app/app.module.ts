import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CountriesComponent } from './pages/countries/countries.component';
import { DetailCountryComponent } from './pages/detail-country/detail-country.component';
import { HeaderComponent } from './components/header/header.component';
import { FiltroComponent } from './components/filtro/filtro.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { TemaModule } from './tema/tema.module';
import { lightTheme } from './tema/light-theme';
import { darkTheme } from './tema/dark-theme';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { FiltroCountriesPipe } from './pipes/filtro-countries.pipe';
import { FormsModule } from '@angular/forms';
import { FiltroRegionPipe } from './pipes/filtro-region.pipe';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    DetailCountryComponent,
    HeaderComponent,
    FiltroComponent,
    BuscadorComponent,
    FiltroCountriesPipe,
    FiltroRegionPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    TemaModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'light'
    }),
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
