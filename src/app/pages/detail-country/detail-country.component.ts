import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { AppComponent } from 'src/app/app.component';
import { country } from 'src/app/models/countries';
import { ContriesService } from 'src/app/services/contries.service';

@Component({
  selector: 'app-detail-country',
  templateUrl: './detail-country.component.html',
  styleUrls: ['./detail-country.component.scss']
})
export class DetailCountryComponent implements OnInit {

  nameCountry : string ="";
  country: country = {
    alpha2Code: "string",
    alpha3Code: "string",
    altSpellings: [],
    area: 0,
    borders: [],
    callingCodes: [],
    capital: "",
    cioc: "",
    currencies: [],
    demonym: "",
    flag: "",
    flags: {
        png: "",
        svg: "",
    },
    independent: false,
    languages: [],
    latlng: [],
    name: "",
    nativeName: "",
    numericCode: "",
    population: 0,
    region: "",
    regionalBlocs: [],
    subregion: "",
    timezones: [],
    topLevelDomain: [],
    translations: {
        br: "",
        de: "",
        es: "",
        fa: "",
        fr: "",
        hr: "",
        hu: "",
        it: "",
        ja: "",
        nl: "",
        pt: "",
    }
  };

  constructor(private _serviceCountries: ContriesService, public _load: AppComponent, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let name = ""
    this.route.params.subscribe(
      (params: Params) => {
        name = params['name']
      }
    )
    this._load.startLoad = true;
    this._serviceCountries.getInfoCountry(name).toPromise().then((res: any) => {
      this._load.startLoad = false;
      if (res.status == 404) {
        this.router.navigate(['/countries'])
      }
      this.country = res[0];
    })
  }

}
