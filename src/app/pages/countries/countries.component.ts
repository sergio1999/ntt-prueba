import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { country } from 'src/app/models/countries';
import { ContriesService } from 'src/app/services/contries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {

  listadoPaises: Array<country> = [];
  nombreBuscador: string = "";
  regionBuscador: string = "";
  listadoRegiones: Array<string> = [];

  constructor(private _serviceCountries: ContriesService, public _load: AppComponent) { }

  ngOnInit(): void {
    this._load.startLoad = true;
    this._serviceCountries.getCountries().toPromise().then((res: any) => {
      this._load.startLoad = false;
      this.listadoPaises = res;
      let regiones: any = [];
      this.listadoPaises.forEach(element => {
        regiones.push(element.region)
      });
      this.listadoRegiones = regiones.filter(this.onlyUnique)
    })
  }

  onlyUnique(value: any, index: any, self: any) {
    return self.indexOf(value) === index;
  }

}
