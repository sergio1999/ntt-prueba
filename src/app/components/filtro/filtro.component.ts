import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.scss']
})
export class FiltroComponent implements OnInit {
  filtro: string = "";
  @Input() listadoRegiones: Array<string> = [];
  @Output() changeBuscador: EventEmitter<string> = new EventEmitter<string>()

  constructor() { }

  ngOnInit(): void {
  }

  selectFiltro(e: any) {
    this.changeBuscador.emit(e)
  }

}
