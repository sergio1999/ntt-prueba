import { Component, OnInit } from '@angular/core';
import { TemaService } from 'src/app/services/tema.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private temaServico: TemaService) { }

  ngOnInit(): void {
  }

  CambiarTema() {
    const active = this.temaServico.getActiveTheme() ;
    if (active.name === 'light') {
      this.temaServico.setTheme('dark');
    } else {
      this.temaServico.setTheme('light');
    }
  }

}
