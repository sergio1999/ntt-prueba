import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroCountries'
})
export class FiltroCountriesPipe implements PipeTransform {

  transform(items: any[], args: string): any {
    let list = [];
    if (args == "") {
      list = items;
    } else {
      items.forEach(element => {
        let nombre = element.name.toLowerCase();
        if (nombre.includes(args.toLowerCase())) {
          list.push(element)
        }
      });
    }

    return list;
  }

}
