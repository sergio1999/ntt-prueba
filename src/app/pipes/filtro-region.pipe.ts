import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroRegion'
})
export class FiltroRegionPipe implements PipeTransform {

  transform(items: any[], args: string): any {
    let list = [];
    if (!args) {
      list = items;
    } else {
      items.forEach(element => {
        if (element.region.includes(args)) {
          list.push(element)
        }
      });
    }
    return list;
  }

}
