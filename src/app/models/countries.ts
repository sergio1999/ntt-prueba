export interface country {
    alpha2Code: string,
    alpha3Code: string
    altSpellings: Array<string>,
    area: number,
    borders: Array<string>,
    callingCodes: Array<string>,
    capital: string,
    cioc: string,
    currencies: Array<currencies>,
    demonym: string
    flag: string
    flags: {
        png: string
        svg: string
    },
    independent: boolean,
    languages: Array<languages>,
    latlng: Array<number>
    name: string
    nativeName: any
    numericCode: string
    population: BigInt | number
    region: string
    regionalBlocs: Array<regionalBlocs>
    subregion: string
    timezones: Array<string>
    topLevelDomain: Array<string>
    translations: {
        br: any
        de: any
        es: any
        fa: any
        fr: any
        hr: any
        hu: any
        it: any
        ja: any
        nl: any
        pt: any
    }
}

export interface currencies {
    code: string
    name: string
    symbol: any
}
export interface languages {
    iso639_1: string
    iso639_2: string
    name: string
    nativeName: any
}
export interface regionalBlocs {
    acronym: string
    name: string
}