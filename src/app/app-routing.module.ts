import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CountriesComponent } from "./pages/countries/countries.component";
import { DetailCountryComponent } from "./pages/detail-country/detail-country.component";

const routes: Routes = [
    { path: '', redirectTo: '/countries', pathMatch: 'full' },
    { path: 'countries', component: CountriesComponent },
    { path: 'country/:name', component: DetailCountryComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: true,
            relativeLinkResolution: 'legacy'
        })
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }